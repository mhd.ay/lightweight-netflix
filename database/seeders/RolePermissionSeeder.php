<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class RolePermissionSeeder extends Seeder
{
    private $userPermissions = [
        'show all movies', 'show movies',
        'show all favourites', 'create favourites', 'destroy favourites',
        'show all watched movies', 'create watched movies',
        'show all rate review movies', 'create rate review movies'
    ];

    private $adminPermissions = [
        'show all movies', 'show movies', 'create movies', 'update movies', 'destroy movies', 'create invite'
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $adminRole = Role::findOrCreate('admin', 'web');
        $userRole = Role::findOrCreate('user', 'web');


        foreach ($this->userPermissions as $permission) {
            Permission::findOrCreate($permission, 'web');
        }

        foreach ($this->adminPermissions as $permission) {
            Permission::findOrCreate($permission, 'web');
        }

        $adminRole->syncPermissions($this->adminPermissions);
        $userRole->syncPermissions($this->userPermissions);
    }
}
