<?php

namespace Database\Seeders;

use App\Models\Admin;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Spatie\Permission\Models\Role;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = User::create([
            'full_name' => 'Admin',
            'email' => 'admin@netflix.com',
            'birth_date' => '2000-01-19',
            'password' => "password", // password
            'remember_token' => Str::random(10),
        ]);

        $adminRole = Role::findOrCreate('admin', 'web');
        $admin->syncRoles([$adminRole]);

        $user = User::create([
            'full_name' => 'User',
            'email' => 'user@netflix.com',
            'birth_date' => '2000-01-19',
            'password' => "password", // password
            'remember_token' => Str::random(10),
        ]);

        $userRole = Role::findOrCreate('user', 'web');
        $user->syncRoles([$userRole]);
    }
}
