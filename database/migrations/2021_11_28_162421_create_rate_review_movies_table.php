<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRateReviewMoviesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rate_review_movies', function (Blueprint $table) {
            $table->id();
            $table->text('review')->nullable();
            $table->unsignedTinyInteger('rate');
            $table->foreignId('user_id')->index()->constrained()->cascadeOnDelete();
            $table->foreignId('movie_id')->index()->constrained()->cascadeOnDelete();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rate_review_movies');
    }
}
