<?php

use \App\Http\Controllers\Auth\AuthController;
use Illuminate\Support\Facades\Route;

// AUTH && FCM GROUP
Route::post('register', [AuthController::class, 'register']);
Route::post('login', [AuthController::class, 'login']);
Route::post('reset-password', [AuthController::class, 'resetPassword']);
Route::post('reset-password-confirm', [AuthController::class, 'resetPasswordConfirm']);
