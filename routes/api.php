<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Auth\ProfileController;
use \App\Http\Controllers\{
    MovieController,
    FavouriteMovieController,
    WatchedMovieController,
    RateReviewMovieController,
    InviteController
};

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//AUTHENTICATED ROUTES
Route::middleware('auth:sanctum')->group(function () {
    //Movies CURD
    Route::apiResource('movies',MovieController::class);

    //Favorite ENDPOINT
    Route::get('favourites', [ FavouriteMovieController::class, 'index']);
    Route::post('favourites', [ FavouriteMovieController::class, 'store']);
    Route::delete('favourites/{movieId}', [ FavouriteMovieController::class, 'destroy']);

    //Watched Movie ENDPOINT
    Route::get('watched-movies', [ WatchedMovieController::class, 'index']);
    Route::post('watched-movies', [ WatchedMovieController::class, 'store']);

    //Watched Movie ENDPOINT
    Route::get('rate-review-movies', [ RateReviewMovieController::class, 'index']);
    Route::post('rate-review-movies', [ RateReviewMovieController::class, 'store']);

    //Invite ENDPOINT
    Route::post('invite', [ InviteController::class, 'store']);

    Route::get('profile', [ ProfileController::class, 'profile']);
    Route::put('profile', [ ProfileController::class, 'updateProfile']);
});

Route::post('process-invited/{invite}', [ InviteController::class, 'processInvited']);

require __DIR__.'/auth.php';
