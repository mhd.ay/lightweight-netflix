# Lightweight Netflix

### LARAVEL 8
...

### Installation

1. Open in cmd or terminal app and navigate to this folder
2. Run following commands

```bash
composer install
```

```bash
cp .env.example .env
```

```bash
php artisan key:generate
```

```bash
php artisan storage:link
```

```bash
php artisan migrate --seed
```

```bash
php artisan serve
```

And navigate to generated server link **[localhost](http://127.0.0.1:8000)**

### Mohammed Ayash

...
