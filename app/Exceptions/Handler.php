<?php

namespace App\Exceptions;

use Illuminate\Auth\AuthenticationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Validation\ValidationException;
use Spatie\Permission\Exceptions\UnauthorizedException;
use Throwable;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var string[]
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var string[]
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {
        $this->reportable(function (Throwable $e) {
            //
        });
    }


    public function render($request, Throwable $e)
    {
        if ($e instanceof ValidationException) {
            if ($request->is('api/*')){
                return  response()->json([
                    'result' => 'error',
                    'error_des' => $e->validator->errors(),
                    'error_code' => 1,
                ],400);
            }
        }else if($e instanceof AuthenticationException){
            if ($request->is('api/*')) {
                return response()->json([
                    'result' => 'error',
                    'error_des' => 'you have to login first',
                    'error_code' => 1,
                ], 403);
            }
            return Redirect::back()->withErrors(['message' => 'you have to login first']);
        }else if($e instanceof UnauthorizedException){
            if ( $request->is('api/*') ) {
                return response()->json([
                    'result' => 'error',
                    'error_des' => 'you have no permission to admit this action',
                    'error_code' => 1,
                ], 403);
            }
            return Redirect::back()->withErrors(['message' => 'Forbidden']);
        }else if ($e instanceof ModelNotFoundException){
            if ($request->is('api/*')) {
                return response()->json([
                    'result' => 'error',
                    'error_des' => 'Item Not Found',
                    'error_code' => 1,
                ], 404);
            }
        }

        return parent::render($request, $e);
    }
}
