<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Storage;

class Movie extends Model
{
    use HasFactory;
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'name',
        'cover',
        'description',
        'rate'
    ];


    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array
     */
    protected $hidden = [
        'cover',
        'deleted_at',
    ];

    protected $appends = ['full_Url_cover'];

    public function getFullUrlCoverAttribute()
    {
        return Storage::disk('public')->url('') . $this->cover;
    }

    public function favouriteUsers(): BelongsToMany
    {
        return $this->belongsToMany(User::class, 'favourites');
    }

    public function watchedMovies(): BelongsToMany
    {
        return $this->belongsToMany(User::class, 'watched_movies');
    }

    public function rateReviewMovies(): HasMany
    {
        return $this->hasMany(RateReviewMovie::class);
    }
}
