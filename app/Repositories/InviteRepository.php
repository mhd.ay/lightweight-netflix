<?php

namespace App\Repositories;

use App\Models\Invite;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Carbon\Carbon;

class InviteRepository
{
    public function add(Request $request)
    {
        $invite = new Invite($request->only(['email']));

        return $invite->save();
    }

    public function processInvited($request, $invite)
    {
        if(($invite->email != $request->email) || $invite->accepted) {
            throw new ModelNotFoundException();
        }

        $invite->update(['accepted' => 1, 'accepted_at' => Carbon::now()->toDateTimeString()]);
    }
}
