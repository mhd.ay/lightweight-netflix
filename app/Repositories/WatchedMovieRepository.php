<?php

namespace App\Repositories;

use App\Models\Movie;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class WatchedMovieRepository
{
    public function add(Request $request)
    {
        $user = User::find(Auth::id());

        $user->watchedMovies()->save(Movie::find($request->get('movie_id')));
    }

}
