<?php

namespace App\Repositories;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Password;
use Spatie\Permission\Models\Role;

class UserRepository
{
    public function register(Request $request)
    {
        $user = new User($request->except(['token']));

        $user->save();
        $user->syncRoles(Role::findByName('user', 'web'));
        return $user;
    }

    public function updateProfile(Request $request, $id)
    {
        $user = User::findOrFail($id);
        $user->update($request->except(['token', 'email']));
        return $user;
    }

    public function delete(User $user)
    {
        $user->delete();
    }

    public function getUsers(Request $request): Builder
    {
        $users = User::query();

        if ($search = $request->get('search')) {
            $tokens = convertToSeparatedTokens($search);
            $users = $users->whereRaw("MATCH(full_name, email) AGAINST(? IN BOOLEAN MODE)", $tokens);
        }

        return $users->orderBy('full_name');
    }
}
