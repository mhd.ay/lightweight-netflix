<?php

namespace App\Repositories;

use App\Http\QueryFilter\Name;
use App\Http\QueryFilter\Sort;
use App\Models\Movie;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Pipeline\Pipeline;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Facades\Storage;
use Spatie\Permission\Models\Role;

class MovieRepository
{
    public function add(Request $request)
    {
        $movie = new Movie($request->except(['rate']));

        if ($request->hasFile('cover'))
            $movie->cover = Storage::disk('public')->put('movies', $request->file('cover'));

        $movie->save();
        return $movie;
    }

    public function update(Request $request, $id)
    {
        $movie = Movie::findOrFail($id);
        $movie->update($request->except(['rate']));

        return $movie;
    }

    public function delete(Movie $movie)
    {
        $movie->delete();
    }

    public function getMovies(): Builder
    {
        return app( Pipeline::class)
            ->send(Movie::query())
            ->through([Name::class, Sort::class])
            ->thenReturn();
    }
}
