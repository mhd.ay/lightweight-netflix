<?php

namespace App\Repositories;

use App\Models\Favourite;
use App\Models\Movie;
use App\Models\User;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class FavouriteRepository
{
    public function add(Request $request)
    {
        $user = User::find(Auth::id());

        $user->favouriteMovies()->save(Movie::find($request->get('movie_id')));
    }

    public function delete($movieId)
    {
        $favourite = Favourite::query()->where('user_id', Auth::id())->where('movie_id', $movieId);
        if (! $favourite)
            throw new ModelNotFoundException();

        $favourite->delete();
    }

}
