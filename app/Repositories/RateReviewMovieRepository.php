<?php

namespace App\Repositories;

use App\Models\Movie;
use App\Models\RateReviewMovie;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class RateReviewMovieRepository
{
    public function add(Request $request)
    {
        $user = User::findOrFail(Auth::id());

        $rateReviewMovie= new RateReviewMovie($request->except(['user_id']));

        $user->rateReviewMovies()->save($rateReviewMovie);
    }

}
