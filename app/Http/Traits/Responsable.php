<?php

namespace App\Http\Traits;

use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Storage;

trait Responsable
{
    public function respondSuccess($content = [], $paginator = []): JsonResponse
    {
        return response()->json([
            'result' => 'success',
            'content' => $content,
            'paginator' => $paginator,
        ], 200);
    }

    public function respondError($message): JsonResponse
    {
        return response()->json([
            'result' => 'error',
            'error_des' => $message,
            'error_code' => 1,
        ], 400);
    }

    public function respondOut($message): JsonResponse
    {
        return response()->json([
            'result' => 'error',
            'error_des' => $message,
            'error_code' => -1,
        ]);
    }

    public function respondMessage($message): JsonResponse
    {
        return response()->json([
            'result' => 'success',
            'error_des' => $message,
            'error_code' => 0,
        ]);
    }
}
