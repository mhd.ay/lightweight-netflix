<?php

namespace App\Http\Controllers;

use App\Http\Requests\FavouriteRequest;
use App\Models\Favourite;
use App\Repositories\FavouriteRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class FavouriteMovieController extends ApiController
{
    private $favouriteRepository;
    public $resource = 'favourite';


    public function __construct(FavouriteRepository $favouriteRepository)
    {
        $this->assignPermissions();
        $this->favouriteRepository = $favouriteRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        $favourites = Auth::user()->favouriteMovies;

        return $this->respondSuccess($favourites);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param FavouriteRequest $request
     * @return JsonResponse
     */
    public function store(FavouriteRequest $request): JsonResponse
    {
        $this->favouriteRepository->add($request);
        return $this->respondSuccess();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $movieId
     * @return JsonResponse
     */
    public function destroy($movieId): JsonResponse
    {
        $this->favouriteRepository->delete($movieId);
        return $this->respondSuccess();
    }
    private function assignPermissions()
    {
        $this->middleware('permission:show all favourites', ['only' => ['index']]);
        $this->middleware('permission:create favourites', ['only' => ['store']]);
        $this->middleware('permission:destroy favourites', ['only' => ['destroy']]);
    }
}
