<?php

namespace App\Http\Controllers;

use App\Http\Controllers\ApiController;
use App\Http\Requests\MovieRequest;
use App\Models\Movie;
use App\Repositories\MovieRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class MovieController extends ApiController
{
    private $movieRepository;
    public $resource = 'movie';


    public function __construct(MovieRepository $movieRepository)
    {
        $this->assignPermissions();
        $this->movieRepository = $movieRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function index(Request $request): JsonResponse
    {
        $limit = $request->get('limit') ? : 10 ;

        $movies = $this->movieRepository->getMovies();

        $movies = $movies->paginate($limit);

        return $this->respondSuccess($movies->map(function ($movie) {
            return collect($movie->toArray())
                ->except(['description'])
                ->all();
        }), $this->createApiPaginator($movies));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param MovieRequest $request
     * @return JsonResponse
     */
    public function store(MovieRequest $request): JsonResponse
    {
        $this->movieRepository->add($request);
        return $this->respondSuccess();
    }

    /**
     * Display the specified resource.
     *
     * @param Movie $movie
     * @return JsonResponse
     */
    public function show(Movie $movie): JsonResponse
    {
        return $this->respondSuccess($movie->load(['rateReviewMovies']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param MovieRequest $request
     * @param Movie $movie
     * @return JsonResponse
     */
    public function update(MovieRequest $request, Movie $movie): JsonResponse
    {
        $this->movieRepository->update($request, $movie);
        return $this->respondSuccess($movie);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Movie $movie
     * @return JsonResponse
     */
    public function destroy(Movie $movie): JsonResponse
    {
        $this->movieRepository->delete($movie);
        return $this->respondSuccess();
    }
    private function assignPermissions()
    {
        $this->middleware('permission:show all movies', ['only' => ['index']]);
        $this->middleware('permission:show movies', ['only' => ['show']]);
        $this->middleware('permission:create movies', ['only' => ['store']]);
        $this->middleware('permission:update movies', ['only' => ['update']]);
        $this->middleware('permission:destroy movies', ['only' => ['destroy']]);
    }
}
