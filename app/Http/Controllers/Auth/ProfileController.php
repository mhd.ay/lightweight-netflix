<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\ApiController;
use App\Http\Requests\User\UserRequest;
use App\Repositories\UserRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;

class ProfileController extends ApiController
{
    private $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function profile(): JsonResponse
    {
        return $this->respondSuccess(Auth::user()->load('favouriteMovies'));
    }

    public function updateProfile(UserRequest $request): JsonResponse
    {
        $user = $this->userRepository->updateProfile($request, Auth::id());
        return $this->respondSuccess($user);
    }

}
