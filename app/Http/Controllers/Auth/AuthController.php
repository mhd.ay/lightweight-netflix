<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\ApiController;
use App\Http\Requests\User\LoginUserRequest;
use App\Http\Requests\User\ResetPasswordConfirmRequest;
use App\Http\Requests\User\ResetPasswordRequest;
use App\Http\Requests\User\UserRequest;
use App\Models\User;
use App\Repositories\UserRepository;
use Illuminate\Http\JsonResponse;

class AuthController extends ApiController
{
    private $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function register(UserRequest $request): JsonResponse
    {
        $user = $this->userRepository->register($request);

        if (!$user->token) {
            $token = $user->createToken('API');
            $user->token = $token->plainTextToken;
            $user->save();
        }

        return $this->respondSuccess($user);
    }

    public function login(LoginUserRequest $request): JsonResponse
    {
        $credentials = $request->only('email', 'password');
        if (auth()->attempt($credentials)) {
            $user = User::whereId(auth()->id())->first();
            if (!$user->token) {
                $token = $user->createToken('API');
                $user->token = $token->plainTextToken;
                $user->save();
            }
            return $this->respondSuccess([
                'user' => $user,
                'token' => $user->token,
            ]);
        }
        return $this->respondError('email or password invalid');
    }

    public function resetPassword(ResetPasswordRequest $request): JsonResponse
    {
        $user = User::whereEmail($request->get('email'))->first();
        $user->code = 123456; //rand(100000,999999);
        $user->save();
        //send password here by SMS or Email

        return $this->respondSuccess($user);
    }

    public function resetPasswordConfirm(ResetPasswordConfirmRequest $request): JsonResponse
    {
        $user = User::whereEmail($request->get('email'))->first();

        if ($user->code != $request->get('code')) {
            return $this->respondError(__('api.error_code'));
        }

        $user->save();

        return $this->respondSuccess($user);
    }

}
