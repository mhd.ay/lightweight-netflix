<?php

namespace App\Http\Controllers;

use App\Http\Requests\WatchedMovieRequest;
use App\Repositories\WatchedMovieRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;

class WatchedMovieController extends ApiController
{
    private $watchedMovieRepository;
    public $resource = 'watchedMovie';


    public function __construct(WatchedMovieRepository $watchedMovieRepository)
    {
        $this->assignPermissions();
        $this->watchedMovieRepository = $watchedMovieRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        $watchedMovies = Auth::user()->watchedMovies;

        return $this->respondSuccess($watchedMovies);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param WatchedMovieRequest $request
     * @return JsonResponse
     */
    public function store(WatchedMovieRequest $request): JsonResponse
    {
        $this->watchedMovieRepository->add($request);
        return $this->respondSuccess();
    }

    private function assignPermissions()
    {
        $this->middleware('permission:show all watched movies', ['only' => ['index']]);
        $this->middleware('permission:create watched movies', ['only' => ['store']]);
    }
}
