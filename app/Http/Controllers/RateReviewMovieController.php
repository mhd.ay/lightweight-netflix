<?php

namespace App\Http\Controllers;

use App\Http\Requests\RateReviewMovieRequest;
use App\Repositories\RateReviewMovieRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;

class RateReviewMovieController extends ApiController
{
    private $rateReviewMovieRepository;
    public $resource = 'rateReviewMovie';


    public function __construct(RateReviewMovieRepository $rateReviewMovieRepository)
    {
        $this->assignPermissions();
        $this->rateReviewMovieRepository = $rateReviewMovieRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        $rateReviewMovies = Auth::user()->rateReviewMovies->load(['user', 'movie']);

        return $this->respondSuccess($rateReviewMovies);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param RateReviewMovieRequest $request
     * @return JsonResponse
     */
    public function store(RateReviewMovieRequest $request): JsonResponse
    {
        $this->rateReviewMovieRepository->add($request);
        return $this->respondSuccess();
    }

    private function assignPermissions()
    {
        $this->middleware('permission:show all rate review movies', ['only' => ['index']]);
        $this->middleware('permission:create rate review movies', ['only' => ['store']]);
    }
}
