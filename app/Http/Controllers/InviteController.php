<?php

namespace App\Http\Controllers;

use App\Http\Requests\InviteRequest;
use App\Http\Requests\ProcessInviteRequest;
use App\Models\Invite;
use App\Repositories\InviteRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rules\In;

class InviteController extends ApiController
{
    private $inviteRepository;
    public $resource = 'invite';


    public function __construct(InviteRepository $inviteRepository)
    {
        $this->assignPermissions();
        $this->inviteRepository = $inviteRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @param ProcessInviteRequest $request
     * @param Invite $invite
     * @return JsonResponse
     */
    public function processInvited(ProcessInviteRequest $request, Invite $invite): JsonResponse
    {
        $this->inviteRepository->processInvited($request, $invite);
        return $this->respondSuccess();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param InviteRequest $request
     * @return JsonResponse
     */
    public function store(InviteRequest $request): JsonResponse
    {
        $this->inviteRepository->add($request);
        return $this->respondSuccess();
    }

    private function assignPermissions()
    {
        $this->middleware('permission:create invite', ['only' => ['store']]);
    }
}
