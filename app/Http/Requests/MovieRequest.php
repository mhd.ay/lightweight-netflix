<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MovieRequest extends FormRequest
{
    /**
     * Determine if the movie is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        switch($this->method()) {
            case 'GET':
            case 'DELETE':
                return [];
            case 'POST':
                return [
                    'name' => 'required|max:250',
                    'description' => 'required',
                    'cover' => 'required|mimes:mp4,mov,ogg|max:20000',
                ];
            case 'PUT':
            case 'PATCH':
            return [
                'name' => 'required|max:250',
                'description' => 'required',
                'cover' => 'mimes:mp4,mov,ogg|max:20000',
            ];
            default:break;
        }
        return [];

    }
}
