<?php

namespace App\Http\Requests;

use App\Models\Favourite;
use App\Models\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;

class ProcessInviteRequest extends FormRequest
{
    /**
     * Determine if the movie is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        switch($this->method()) {
            case 'GET':
            case 'DELETE':
                return [];
            case 'POST':
                return [
                    'full_name' => 'required|max:250',
                    'email' => 'required|email|unique:users',
                    'password' => 'required|confirmed|min:6',

                ];
            case 'PUT':
            case 'PATCH':
            default:break;
        }
        return [];

    }
}
