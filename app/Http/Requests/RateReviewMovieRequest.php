<?php

namespace App\Http\Requests;

use App\Models\Favourite;
use App\Models\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;

class RateReviewMovieRequest extends FormRequest
{
    /**
     * Determine if the movie is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        switch($this->method()) {
            case 'GET':
            case 'DELETE':
                return [];
            case 'POST':
                return [
                    'movie_id' => [
                        'required',
                        Rule::exists('movies','id')->whereNull('deleted_at'),
                        function ($attribute, $value, $fail) {
                            $movieIDs = User::find(Auth::id())->watchedMovies->pluck('id');
                            $rateIDs = User::find(Auth::id())->rateReviewMovies->pluck('movie_id');

                            if(! $movieIDs->contains($value))
                                $fail('The selected movie is not watched before.');

                            if( $rateIDs->contains($value))
                                $fail('The selected movie is reviewed before.');
                        }
                    ],
                    'rate' => 'required|integer|between:0,10'
                ];
            case 'PUT':
            case 'PATCH':
            default:break;
        }
        return [];

    }
}
