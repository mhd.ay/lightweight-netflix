<?php

namespace App\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        switch($this->method()) {
            case 'GET':
            case 'DELETE':
                return [];
            case 'POST':
                return [
                    'full_name' => 'required|max:250',
                    'email' => 'required|email|unique:users',
                    'birth_date' => 'required|date',
                    'password' => 'required|confirmed|min:6',
                ];
            case 'PUT':
            case 'PATCH':
                return [
                    'full_name' => 'required|max:250',
                    'birth_date' => 'required|date',
                    'password' => 'required|confirmed|min:6',
                ];
            default:break;
        }
        return [];

    }
}
