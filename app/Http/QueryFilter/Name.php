<?php


namespace App\Http\QueryFilter;

class Name extends Filter
{
    protected function applyFilter($builder)
    {
        $tokens = convertToSeparatedTokens(request()->get($this->filterName()));
        return $builder->whereRaw("MATCH(name) AGAINST(? IN BOOLEAN MODE)", $tokens);
    }

}
