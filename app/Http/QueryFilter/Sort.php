<?php


namespace App\Http\QueryFilter;

class Sort extends Filter
{
    protected function applyFilter($builder)
    {
        return $builder->orderBy(request()->get($this->filterName()));
    }

}
