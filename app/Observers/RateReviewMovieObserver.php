<?php

namespace App\Observers;

use App\Models\RateReviewMovie;
use App\Models\User;
use Illuminate\Support\Str;

class RateReviewMovieObserver
{

    /**
     * Handle the User "creating" event.
     *
     * @param RateReviewMovie $rateReviewMovie
     * @return void
     */
    public function created(RateReviewMovie $rateReviewMovie)
    {
        $movie = $rateReviewMovie->movie;
        $movie->rate = $movie->rateReviewMovies->sum('rate') / $movie->rateReviewMovies->count();
        $movie->save();
    }

}
