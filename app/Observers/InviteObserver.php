<?php

namespace App\Observers;

use App\Mail\InviteMail;
use App\Models\Invite;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;
use Spatie\Permission\Models\Role;

class InviteObserver
{
    /**
     * @var Request
     */
    private $request;

    /**
     * InviteObserver constructor.
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * Handle the User "creating" event.
     *
     * @param Invite $invite
     * @return void
     */
    public function creating(Invite $invite)
    {
        $invite->token = $this->generateToken();

        $details = [
            'title' => 'Hello we hop if you join with us as Admin',
            'body' => 'This is the token that you will used for continue Register process,
                        token is '. $invite->token
        ];

        Mail::to($this->request->get('email'))->send(new InviteMail($details));
    }

    /**
     * Handle the User "updating" event.
     *
     * @param Invite $invite
     * @return void
     */
    public function updating(Invite $invite)
    {
        $user = User::create([
            'full_name' => $this->request->get('full_name'),
            'email' => $invite->email,
            'password' => $this->request->get('password'),
        ]);
        $user->syncRoles(Role::findByName('admin', 'web'));
    }

    /**
     * Generate random token, check if unique, if not regenerate.
     *
     * @return string $token
     */
    protected function generateToken()
    {
        $token = Str::random(30);
        if(Invite::where('token', $token)->first()) {
            return $this->generateToken();
        }
        return $token;
    }
}
