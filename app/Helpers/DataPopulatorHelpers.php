<?php

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;

function populateModelData(Request $request, $model): array
{
    $model = app($model);
    $data = [];
    if ($model->translatedAttributes){
        foreach (config('translatable.locales') as $locale){
            foreach ($model->translatedAttributes as $attribute){
                $value = $request->get($attribute);
                if (isset($value))
                    $data[$locale][$attribute] = $request->input($attribute);
            }
        }
    }

    foreach ($model->getFillable() as $item){
        $value = $request->get($item);
        if (isset($value))
            $data[$item] = $request->input($item);
    }

    return $data;
}
